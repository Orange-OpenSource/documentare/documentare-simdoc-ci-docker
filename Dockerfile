FROM debian:bookworm

LABEL maintainer="christophe.maldivi@orange.com"

# git for 'git describe' to retrieve tag; opencv; image magick; graphviz; raw-converter
# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    wget \
    curl \
    jq \
    make \
    build-essential \
    devscripts \
    fakeroot \
    debhelper \
    git \
    openjdk-17-jdk-headless \
    maven \
    bzip2 \
    imagemagick gsfonts \
    graphviz && \
    \
    wget https://gitlab.com/Orange-OpenSource/documentare/image-converter/-/jobs/5322206399/artifacts/raw/raw-converter_10.0.0_amd64.deb && dpkg -i raw-converter_10.0.0_amd64.deb; apt-get -f install --no-install-recommends -y && \
    \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# TEST GRAPHVIZ
RUN dot -V
RUN gvmap -V
# TEST RAW CONVERTER
RUN which raw-converter

# RUN AS USER
RUN useradd -ms /bin/bash user
USER user
WORKDIR /home/user
